package br.com.daboiud.nataguava.models;

import lombok.Data;

@Data
public class Content {

    private ContentTag ContentTag;
    private int qtQuestions;

}